# Test app

To run the app use `docker-compose up -d` command.

 - set transaction example
 `curl -X POST -H "Source-Type:client" -d '{"amount":"11.33","transaction_id":"1423","state":"lost"}' http://localhost:8080/username`
 
 - Set `ODD_DURATION` env var to provide time period for "ten odd" operation
 
# TODO:
 - proper tests
 - validators
 - linters
 - api "to get" balance
 - cache
 - etc