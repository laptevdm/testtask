FROM golang:1.14.1

WORKDIR /app
COPY . .
RUN go mod download && go build -o app .
EXPOSE 8080
CMD ["./app"]