CREATE TABLE balance (
    "id"             SERIAL PRIMARY KEY,
	"user"           VARCHAR (256) NOT NULL,
	"source"         VARCHAR (256) NOT NULL,
	"amount"         NUMERIC(17,2) NOT NULL,
	"state"          VARCHAR (256) NOT NULL,
	"transaction_id" VARCHAR (256) NOT NULL,
	"balance"        NUMERIC(17,2) NOT NULL,
	"created_at"     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	"updated_at"     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	"status"         VARCHAR (256) NULL
);

CREATE INDEX user_idx ON balance ("user");
CREATE UNIQUE INDEX source_id_idx ON balance ("source", "transaction_id");

