module test

go 1.14

require (
	github.com/docker/docker v1.4.2-0.20200213202729-31a86c4ab209
	github.com/golang-migrate/migrate/v4 v4.10.0
	github.com/jteeuwen/go-bindata v3.0.7+incompatible // indirect
	github.com/lib/pq v1.3.0
	github.com/shopspring/decimal v0.0.0-20200227202807-02e2044944cc
	github.com/stretchr/testify v1.4.0
)
