package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"strings"
)

func bindata_read(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	return buf.Bytes(), nil
}

var __1_migr_down_sql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x01\x00\x00\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00")

func _1_migr_down_sql() ([]byte, error) {
	return bindata_read(
		__1_migr_down_sql,
		"1_migr.down.sql",
	)
}

var __1_migr_up_sql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xa4\x91\x41\x4b\xc3\x30\x14\xc7\xcf\xe6\x53\x3c\x72\x5a\xa1\x17\x07\xea\x61\xa7\xd8\x3d\x31\x98\x66\x33\x4b\xc4\x9d\x42\x6c\x72\x28\x68\x2b\x6d\x0a\x7e\x7c\x89\xeb\xb6\x32\x99\x0a\xe6\xfc\x7b\xff\x97\xf7\xff\x15\x0a\x99\x46\xd0\xec\x56\x20\xbc\xb8\x57\xd7\x54\x01\x66\x04\x00\x80\xd6\x9e\xc2\xf4\x6d\x50\x71\x26\x60\xad\x78\xc9\xd4\x16\x1e\x70\x9b\x93\x0b\x3a\xf4\xa1\x9b\x72\x4f\x4c\x15\xf7\x4c\xc1\x6c\x7e\x75\x9d\x81\x5c\x69\x90\x46\x88\x44\xf6\xed\xd0\x55\x81\xfe\x81\x74\x6f\xed\xd0\xc4\x23\x29\x4d\x89\x8a\x17\xb3\xcb\x9b\x7c\x7e\x92\x19\x5d\x9c\x44\xfe\x90\x19\x3b\xd7\xf4\xae\x8a\x75\xdb\xd8\x74\xd9\x79\x72\xac\x81\xfe\xbe\xbd\xea\x82\x8b\xc1\x5b\x37\xfe\x55\xf3\x12\x37\x9a\x95\xeb\x03\x05\x4b\xbc\x63\x46\x68\x28\x8c\x52\x28\xb5\x3d\x20\x5f\xdd\xbd\xfb\x7f\xcd\xa7\xeb\x87\xfe\x6c\xa3\x46\x08\x92\x2d\x08\x19\x25\x73\xb9\xc4\x67\x48\xbe\x6c\xed\x3f\x60\x25\x8f\xc2\x77\x16\xb3\xc5\x1e\x35\x92\x3f\x9a\xfd\xc4\xce\x9b\xad\xfd\xf7\xb1\x51\x69\x0e\xa7\xf5\xa6\xb5\x9f\x01\x00\x00\xff\xff\x3f\xd5\x1c\x9f\x5c\x02\x00\x00")

func _1_migr_up_sql() ([]byte, error) {
	return bindata_read(
		__1_migr_up_sql,
		"1_migr.up.sql",
	)
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		return f()
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() ([]byte, error){
	"1_migr.down.sql": _1_migr_down_sql,
	"1_migr.up.sql":   _1_migr_up_sql,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for name := range node.Children {
		rv = append(rv, name)
	}
	return rv, nil
}

type _bintree_t struct {
	Func     func() ([]byte, error)
	Children map[string]*_bintree_t
}

var _bintree = &_bintree_t{nil, map[string]*_bintree_t{
	"1_migr.down.sql": &_bintree_t{_1_migr_down_sql, map[string]*_bintree_t{}},
	"1_migr.up.sql":   &_bintree_t{_1_migr_up_sql, map[string]*_bintree_t{}},
}}
