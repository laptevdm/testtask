package main

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/docker/docker/pkg/locker"
	"github.com/shopspring/decimal"
)

type State string

var lock = locker.New()

const (
	Win  = State("win")
	Lost = State("lost")
)

func Add(user, source, tid string, amount decimal.Decimal, state State) error {
	lock.Lock(user)
	defer lock.Unlock(user)

	row := db.QueryRow(`SELECT SUM(balance) FROM balance WHERE "user"=$1`, user)

	var balance decimal.NullDecimal
	operationBalance := amount

	if err := row.Scan(&balance); err != nil && err != sql.ErrNoRows {
		return err
	}

	if state == Lost && balance.Decimal.Sub(amount).IsNegative() {
		return fmt.Errorf("insufficient balance")
	}

	if state == Lost {
		operationBalance = amount.Neg()
	}

	_, err := db.Exec(`INSERT INTO balance("user","source","transaction_id", "amount", "state", "balance") VALUES($1,$2,$3,$4,$5,$6)`,
		user, source, tid, amount, state, operationBalance)

	return err
}

func Cancel(user string, id int) error {
	lock.Lock(user)
	defer lock.Unlock(user)

	res, err := db.Exec(`UPDATE balance SET "balance"=0, "status"='cancelled', "updated_at"=now() WHERE id=$1 AND "status" IS NULL`, id)
	if err != nil {
		return nil
	}

	if aff, err := res.RowsAffected(); aff == 0 || err != nil {
		return nil
	}

	// get balance and apply correction if it is negative
	row := db.QueryRow(`SELECT SUM("balance") FROM balance WHERE "user"=$1`, user)

	var balance decimal.Decimal

	if err := row.Scan(&balance); err != nil && err != sql.ErrNoRows {
		return err
	}

	if balance.IsNegative() {
		_, err := db.Exec(`INSERT INTO balance("user","source","transaction_id", "amount", "state", "balance", "status") VALUES($1,$2,$3,$4,$5,$6)`,
			user, `correction`, time.Now().Format(time.RFC3339Nano), balance.Neg(), ``, balance.Neg(), `correction`)

		if err != nil {
			return err
		}
	}

	return nil
}

func TenOdd() error {
	rows, err := db.Query(`SELECT b.id, b."user" FROM (SELECT id, "user", row_number() OVER(ORDER BY id DESC) AS row FROM balance WHERE "status" !='cancelled') b WHERE b.row % 2 = 0 LIMIT 10`)
	if err != nil {
		return err
	}
	defer rows.Close()

	var user string
	var id int

	for rows.Next() {
		if err := rows.Scan(&id, &user); err != nil {
			return err
		}

		if err = Cancel(user, id); err != nil {
			return err
		}
	}

	return nil
}
