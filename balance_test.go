package main

import (
	"fmt"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAdd(t *testing.T) {
	var err error
	db, err = prepareDB(fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		getEnv("POSTGRES_HOST", "localhost"),
		getEnv("POSTGRES_PORT", "5432"),
		getEnv("POSTGRES_USER", "user"),
		getEnv("POSTGRES_PASSWORD", "pass"),
		getEnv("POSTGRES_DATABASE", "test"),
		getEnv("POSTGRES_SSLMODE", "disable"),
	))

	assert.NoError(t, err)

	for i := 10; i < 20; i++ {
		err = Add("test", "some", fmt.Sprintf("123%d", i), decimal.NewFromFloat(70.45), Lost)
	}
	assert.NoError(t, err)
}

func TestCancel(t *testing.T) {
	var err error
	db, err = prepareDB(fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		getEnv("POSTGRES_HOST", "localhost"),
		getEnv("POSTGRES_PORT", "5432"),
		getEnv("POSTGRES_USER", "user"),
		getEnv("POSTGRES_PASSWORD", "pass"),
		getEnv("POSTGRES_DATABASE", "test"),
		getEnv("POSTGRES_SSLMODE", "disable"),
	))

	for i := 1; i < 20; i++ {
		err = Cancel("test", i)
	}
	assert.NoError(t, err)
}

func TestTenOdd(t *testing.T) {
	var err error
	db, err = prepareDB(fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		getEnv("POSTGRES_HOST", "localhost"),
		getEnv("POSTGRES_PORT", "5432"),
		getEnv("POSTGRES_USER", "user"),
		getEnv("POSTGRES_PASSWORD", "pass"),
		getEnv("POSTGRES_DATABASE", "test"),
		getEnv("POSTGRES_SSLMODE", "disable"),
	))

	assert.NoError(t, err)

	err = TenOdd()
	assert.NoError(t, err)
}
