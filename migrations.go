package main

import (
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"

	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
)

//go:generate go-bindata -prefix migrations/ -pkg main -o migrations.bindata.go migrations/
func migrateUp() error {
	r := bindata.Resource(AssetNames(), Asset)

	s, err := bindata.WithInstance(r)
	if err != nil {
		return fmt.Errorf("prepare source instance: %s", err)
	}

	cfg := postgres.Config{
		MigrationsTable: "versions",
	}
	d, err := postgres.WithInstance(db, &cfg)
	if err != nil {
		return fmt.Errorf("prepare database instance: %s", err)
	}

	m, err := migrate.NewWithInstance("go-bindata", s, "postgres", d)
	if err != nil {
		return fmt.Errorf("prepare migrate instance: %s", err)
	}

	return m.Up()
}
