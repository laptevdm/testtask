package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/lib/pq"
	"github.com/shopspring/decimal"
)

var db *sql.DB

func main() {

	postgresURL := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		getEnv("POSTGRES_HOST", "pg"),
		getEnv("POSTGRES_PORT", "5432"),
		getEnv("POSTGRES_USER", "user"),
		getEnv("POSTGRES_PASSWORD", "pass"),
		getEnv("POSTGRES_DATABASE", "test"),
		getEnv("POSTGRES_SSLMODE", "disable"),
	)

	var err error

	backOff := time.Second
	for db, err = prepareDB(postgresURL); err != nil; {
		log.Printf("db connect error: %w, backoff %s", err, backOff)
		time.Sleep(backOff)
		if backOff < 5*time.Second {
			backOff += time.Second
		}
		db, err = prepareDB(postgresURL)
	}

	backOff = time.Second
	for err = migrateUp(); err != nil && err != migrate.ErrNoChange; {
		log.Printf("db migrations error: %w, backoff %s", err, backOff)
		time.Sleep(backOff)
		if backOff < 5*time.Second {
			backOff += time.Second
		}
		err = migrateUp()
	}

	http.HandleFunc("/", AddHandler)

	go func() {
		d, err := time.ParseDuration(getEnv("ODD_DURATION", "5s"))
		if err != nil {
			return
		}
		log.Printf("start odd with duration %s", d)
		t := time.NewTicker(d)
		for range t.C {
			if err := TenOdd(); err != nil {
				log.Printf("get odd error: %s", err)
			}
		}
	}()

	log.Printf("start http server on port :8080")
	http.ListenAndServe(":8080", nil)
}

type Request struct {
	State         State           `json:"state"`
	Amount        decimal.Decimal `json:"amount"`
	TransactionID string          `json:"transaction_id"`
}

func AddHandler(w http.ResponseWriter, req *http.Request) {
	dec := json.NewDecoder(req.Body)
	dec.DisallowUnknownFields()

	var p Request
	if err := dec.Decode(&p); err != nil {
		http.Error(w, "wrong json request", http.StatusBadRequest)
		return
	}

	source := req.Header.Get("Source-Type")
	if len(source) == 0 {
		http.Error(w, fmt.Sprintf("empty source"), http.StatusOK)
		return
	}
	user := strings.Trim(req.URL.Path, "/")
	if len(user) == 0 {
		http.Error(w, fmt.Sprintf("empty user"), http.StatusOK)
		return
	}

	if err := Add(user, source, p.TransactionID, p.Amount, p.State); err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}
}

func prepareDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, fmt.Errorf("failed to connect db: %w", err)
	}
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	return db, nil
}

func getEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
